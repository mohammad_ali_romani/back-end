<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker::create();

         foreach(range(1,30) as $index)
         {
             $post = Post::create([
                 'title'=>$faker->title(),
                 'description'=>$faker->paragraph(),
                 'category_id'=>$faker->numberBetween(1,4),
                 'user_id'=>$faker->numberBetween(1,10),
                 'subject_id'=>$faker->numberBetween(1,16),

             ]);
             $post->depts()->attach( [$faker->numberBetween(1,3)]);
             $post->years()->attach( [$faker->numberBetween(1,2)]);
         }


    }
}
