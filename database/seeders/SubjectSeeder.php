<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();


        foreach(range(1,30) as $index)
        {
            DB::table('subjects')->insert([
                'name'=>$faker->name,
                'dept_id'=>$faker->numberBetween(1,3),
                'year_id'=>$faker->numberBetween(1,2),
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);
        }
    }
}
