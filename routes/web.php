<?php

use App\Http\Controllers\Dashborde\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//define('PAGINATION_COUNT',10);

Route::get('/',[AdvertismentController::class,'index']);
// Route::resource('Advertisment', AdvertismentController::class);
// Route::resource('Advertisment',AdvertismentController::class)->name('create','advertisment.create');
//Route::resource('Advertisment/update/{advertisment_id}',AdvertismentController::class);
// Route::get('Advertisment',[AdvertismentController::class,'index']);
// Route::get('index/create',[AdvertismentController::class,'create'])->name('advertisment.create');
// Route::post('index/create/store',[AdvertismentController::class,"store"])->name('advertisment.store');
// Route::get('index/edit',[AdvertismentController::class,'edit'])->name('advertisment.edit');

Route::group(['prefix'=>'Advertisment'],function(){

    Route::get('/',[AdvertismentController::class,'index'])->name('Advertisment.index');
    Route::get('create',[AdvertismentController::class,'create'])->name('Advertisment.create');
    Route::post('create/store',[AdvertismentController::class,'store'])->name('Advertisment.store');
    Route::get('edit/{Advertisment_id}',[AdvertismentController::class,'edit'])->name('Advertisment.edit');
    Route::post('update/{Advertisment_id}',[AdvertismentController::class,'update'])->name('Advertisment.update');
    Route::get('delete/{Advertisment_id}',[AdvertismentController::class,'destroy'])->name('Advertisment.delete');
});

Route::group(['prefix'=>'Lecture'],function(){
    Route::get('/',[LectureController::class,'index'])->name('Lecture.index');
    Route::get('create',[LectureController::class,'create'])->name('Lecture.create');
    Route::post('create/store',[LectureController::class,'store'])->name('Lecture.store');
    Route::get('edit/{Lecture_id}',[LectureController::class,'edit'])->name('Lecture.edit');
    Route::post('update/{Lecture_id}',[LectureController::class,'update'])->name('Lecture.update');
    Route::get('delete/{Lecture_id}',[LectureController::class,'destroy'])->name('Lecture.delete');

});

Route::group(['prefix'=>'Mark'],function(){

    Route::get('/',[MarkController::class,'index'])->name('Mark.index');
    Route::get('create',[MarkController::class,'create'])->name('Mark.create');
    Route::post('create/store',[MarkController::class,'store'])->name('Mark.store');
    Route::get('edit/{Mark_id}',[MarkController::class,'edit'])->name('Mark.edit');
    Route::post('update/{Mark_id}',[MarkController::class,'update'])->name('Mark.update');
    Route::get('delete/{Mark_id}',[MarkController::class,'destroy'])->name('Mark.delete');

});
Route::group(['prefix'=>'Program'],function(){

    Route::get('/',[ProgramController::class,'index'])->name('Program.index');
    Route::get('create',[ProgramController::class,'create'])->name('Program.create');
    Route::post('create/store',[ProgramController::class,'store'])->name('Program.store');
    Route::get('edit/{Program_id}',[ProgramController::class,'edit'])->name('Program.edit');
    Route::post('update/{Program_id}',[ProgramController::class,'update'])->name('Program.update');
    Route::get('delete/{Program_id}',[ProgramController::class,'destroy'])->name('Program.delete');

});

Route::group(['prefix'=>'Subject'],function(){

    Route::get('/',[SubjectController::class,'index'])->name('Subject.index');
    Route::get('create',[SubjectController::class,'create'])->name('Subject.create');
    Route::post('create/store',[SubjectController::class,'store'])->name('Subject.store');
    Route::get('edit/{Subject_id}',[SubjectController::class,'edit'])->name('Subject.edit');
    Route::post('update/{Subject_id}',[SubjectController::class,'update'])->name('Subject.update');
    Route::get('delete/{Subject_id}',[SubjectController::class,'destroy'])->name('Subject.delete');

});

Route::group(['prefix'=>'User'],function(){

    Route::get('/',[UserController::class,'index'])->name('User.index');
    Route::get('create',[UserController::class,'create'])->name('User.create');
    Route::post('create/store',[UserController::class,'store'])->name('User.store');
    Route::get('edit/{User_id}',[UserController::class,'edit'])->name('User.edit');
    Route::post('update/{User_id}',[UserController::class,'update'])->name('User.update');
    Route::get('delete/{User_id}',[UserController::class,'destroy'])->name('User.delete');

});

Route::get('test',[PostController::class,'test']);



