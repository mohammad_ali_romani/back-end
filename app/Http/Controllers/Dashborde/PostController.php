<?php

namespace App\Http\Controllers\Dashborde;

use App\Models\Post;
use Illuminate\Http\Request;

/* begin Test */

use App\Models\Subject;
use App\Models\Dept;
use App\Models\Year;
use App\Models\Category;
/* end test */

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }


    public function test()
    {
       //$subjects=Subject::with('dept','year')->get();
       // return $subjects;

    //    $dept_subject = Subject::find(6);
    //    return $dept_subject->dept;

        // $year_subject=Subject::find(1);
        // return $year_subject->year;

        // $subjects_dept= Dept::with('subjects')->get();
        // return $subjects_dept;

        // $subjects_year=Year::with('subjects')->get();
        // return $subjects_year;

       $post= Post::with('subject')->find(1);
       return $post;
    }
}
