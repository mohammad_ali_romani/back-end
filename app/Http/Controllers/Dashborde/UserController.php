<?php

namespace App\Http\Controllers\Dashborde;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUsers = User::select('name','email','password','type')->paginate(10);
        return view('User.AllUser',compact('allUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('User.createUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //Validation

        //insert data into tabel
        User::create([
            'name'=> $request->name,
            'email'=> $request->email,
            'password'=>$request->password,
            'type'=>$request->type
        ]);    
        

        //retrun into index page
        return redirect()->route('User.create')->with(['success'=>'تم إضافة البيانات بنجاح']); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(User $uesr)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit($User_id)
    {
        $user= User::find($User_id);
        if(!$user)
        {
            return redirect()->route('User.index')->with(['error'=>'المستخدم غير موجود']);
        }
        return view('User.editUser',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$User_id)
    {
        $uesr = User::find($User_id);
        // check if id exiet
        if(!$uesr)
        {
            return redirect()->route('User.index')->with(['error'=>' المستخدم غير موجود']);
        }
        //update data
        $uesr->update($request->all());

        //return into Advertisment page 
        return redirect()->route('User.index')->with(['success'=>'تم تعديل البيانات بنجاح']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($User_id)
    {
        $user= User::find($User_id);
        // check if id exiet
        if(!$user)
        {
            return redirect()->route('User.index')->with(['error'=>'المستخدم غير موجود']);
        }
        $user->delete();
        return redirect()->route('User.index')->with(['success'=>'تم حذف البيانات بنجاح']);
    }
}
