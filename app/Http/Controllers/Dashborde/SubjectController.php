<?php

namespace App\Http\Controllers\Dashborde;

use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allSubject = Subject::select('name')->paginate(10);
        return view('Subject.AllSubject',compact('allSubject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Subject.createSubject');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

          //Validation
  
          //insert data into tabel
        Subject::create([
              'name'=> $request->name
          ]);
          
  
          //retrun into index page
          return redirect()->route('Subject.create')->with(['success'=>'تم إضافة البيانات بنجاح']); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit($Subject_id)
    {
        $subject= Subject::find($Subject_id);
        if(!$subject)
        {
            return redirect()->route('Subject.index')->with(['error'=>'المادة غير موجودة']);
        }
        return view('Subject.editSubject',compact('subjeck'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$Subject_id)
    {
        $subject = Subject::find($Subject_id);
        // check if id exiet
        if(!$subject)
        {
            return redirect()->route('Subject.index')->with(['error'=>' المادة غير موجودة']);
        }
        //update data
        $subject->update($request->all());

        //return into Advertisment page 
        return redirect()->route('Subject.index')->with(['success'=>'تم تعديل البيانات بنجاح']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy($Subject_id)
    {
        $subject= Subject::find($Subject_id);
        // check if id exiet
        if(!$subject)
        {
            return redirect()->route('Subject.index')->with(['error'=>'المادة غير موجودة']);
        }
        $subject->delete();
        return redirect()->route('Subject.index')->with(['success'=>'تم حذف البيانات بنجاح']);
    }
}
