<?php
namespace App\Traits;

Trait DashbordeTrait
{
    
    
    function saveFile($file,$folder)
    {

        $fileExtension= $file->getClientOriginalExtension();
        $fileNameClient=$file->getClientOriginalName();
        $fileName=$fileNameClient.'_'.time().'.'.$fileExtension;
        $file->move($folder,$fileName);
        return $fileName;
    }
    function fileType($file)
    {
        $fileExtension =$file->getClientOriginalExtension();
        return $fileExtension;
    }
    
}