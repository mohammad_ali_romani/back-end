@extends('layout.layout')
@section('title')
المحاضرات
@endsection
@section('content')
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
            <h2>الاعلانات</h2>
            <a href="{{ route('Advertisment.create') }}" class="user btn btn-info">إضافة اعلان</a>
        </thead>

    </table>
</div>
<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">القسم</th>
                <th scope="col">السنة</th>
                <th scope="col">نص الاعلان</th>
                <th scope="col"> خصائص </th>

            </tr>
        </thead>
        <tbody>

            @php
            $i = 0;
            @endphp
            @foreach ($allAdvertisments as $Advertisment)
            <tr>
                @php
                $i++;
                @endphp
                <td>{{ $i }}</td>

                <td>
                    @if ($Advertisment->depts->count() == 1)
                    {{ $Advertisment->depts[0]->name }}
                    @else
                    @foreach ($Advertisment->depts as $dept)

                    {{ $dept->name . ',' }}


                    @endforeach
                    @endif

                </td>
                <td>
                    @if ($Advertisment->years->count() == 1)
                    {{ $Advertisment->years[0]->name }}
                    @else
                    @foreach ($Advertisment->years as $year)
                    {{ $year->name . ',' }}
                    @endforeach
                    @endif
                </td>
                <td>{{ $Advertisment->text }}</td>
                <td>
                    <a href="{{ route('Advertisment.edit', $Advertisment->id) }}" class="btn btn-success">تعديل</a>
                    <a href="{{ route('Advertisment.delete', $Advertisment->id) }}" class="btn btn-danger">حذف</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection