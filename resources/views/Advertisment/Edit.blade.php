@extends('layout.layout')

@section('content')

    <div class="mb-4">
        <h1>تعديل إعلان</h1>

        <form method="POST" action="{{ route('Advertisment.update', $ad->id) }}" enctype="multipart/form-data">
            @csrf
            @method('POST')
            @foreach ($depts as $dept)
                <div class="form-check">

                    <input class="form-check-input" type="checkbox" value="{{ $dept->id }}" @foreach ($ad_depts as $ad_dept)
                    @if ($ad_dept->id == $dept->id)
                        checked
                    @endif
            @endforeach
            name="depts[]">
            <label class="form-check-label" for="defaultCheck1">
                {{ $dept->name }}
            </label>
    </div>
    @endforeach
    <label for="dept" class="form-label">السنة </label>

    @foreach ($years as $year)
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="{{ $year->id }}" name="years[]" @foreach ($ad_years as $ad_year)
            @if ($ad_year->id == $year->id)
                checked
            @endif
    @endforeach
    >
    <label class="form-check-label" for="defaultCheck1">
        {{ $year->name }}
    </label>
    </div>
    @endforeach
    <div class="mb-3">
        <label for="formFile" class="form-label">اضف ملف جديد</label>
        <input class="form-control" type="file" id="formFile" name="file">
    </div>
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">النص</label>
        <textarea name="text" cols="30" rows="10" class="form-control">{{ $ad->text }}</textarea>
    </div>

    <div>
        <button type="submit" class="btn btn-primary mb-3">تعديل</button>
    </div>
    </form>

@endsection
