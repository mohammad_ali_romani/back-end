@extends('layout.layout')

@section('content')
    <div class="mb-4">
        <h1>إضافة إعلان</h1>
        <form method="POST" action="{{ route('Advertisment.store') }}" enctype="multipart/form-data">
            @csrf
            <label for="dept" class="form-label f-1">القسم </label>
            @foreach ($depts as $dept)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value={{ $dept->id}} name="depts[]">
                    <label class="form-check-label" for="defaultCheck1">
                        {{ $dept->name }}
                    </label>
                </div>
            @endforeach
            <label for="dept" class="form-label">السنة </label>
            @foreach ($years as $year)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value={{ $year->id }} name="years[]"/>
                    <label class="form-check-label" for="defaultCheck1">
                        {{ $year->name }}
                    </label>
                </div>
            @endforeach
            <div class="mb-3">
                <label for="formFile" class="form-label"> إختر الملف</label>
                <input class="form-control" type="file" id="formFile" name="file">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">النص</label>
                <textarea name="text" cols="30" rows="8" placeholder="النص" class="form-control"></textarea>
            </div>
            <div>
                <button type="submit" class="btn btn-primary mb-3">إضافة</button>
            </div>

        </form>
    </div>
@endsection
