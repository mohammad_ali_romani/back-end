@extends('layout.layout')
@section('title')
البرامج
@endsection
@section('content')
<div class="table-responsive">
    <table class="table table-striped table-sm">
     <thead>
      <h2>الاعلانات</h2>
       <a href="{{route('Advertisment.create')}}" class="user btn btn-info" >إضافة علامة</a>
    </thead>

    </table>
  </div>
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">القسم</th>
          <th scope="col">السنة</th>
          <th scope="col">نص العلامة</th>
          <th scope="col"> خصائص </th>

        </tr>
      </thead>
      <tbody>
            @php
                $i = 0;
            @endphp
         @foreach ($allPrograms as $Program)
        <tr>        
            @php
              $i++;
            @endphp
                <td>{{$i}}</td>

                <td>
                  @if ($Program->depts->count() == 1)
                                {{ $Program->depts[0]->name }}
                            @else
                                @foreach ($Program->depts as $dept)

                                    {{ $dept->name . ',' }}


                                @endforeach
                   @endif
                </td>
                <td>
                  @if ($Program->years->count() == 1)
                  {{ $Program->years[0]->name }}
              @else
                  @foreach ($Program->years as $year)
                      {{ $year->name . ',' }}
                  @endforeach
              @endif
                </td>
                <td>{{$Program->type}}</td>

          <td><a href="{{route('Program.edit',$Program->id)}}" class="btn btn-success">تعديل</a>

            <a href="{{route('Program.delete',$Program->id)}}" class="btn btn-danger">حذف</a></td>

    </td>
    @endforeach
      </tbody>
    </table>
  </div>
</main>
</div>
</div>

@endsection

